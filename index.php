<?php
function questioneOne($str, $n){
    $n = ($n<10) ? 10 : $n;
    $str = strtolower(str_repeat($str, $n));
    $str = (strlen($str) > 10) ? substr($str,0,10) : $str;
    echo "String is: " . $str . "<br>" . "Count a: " . substr_count($str, 'a');;
}
echo "Question 1:" . "<br>";
questioneOne("abcac", 8);
echo "<hr>";

function questioneTwo($str){
    $count = 0;
    for($i=0; $i<strlen($str)-1;$i++ ){
        if( $str[$i] == $str[$i+1] ){
            $count++;
        }
    }
    echo $count;
}
echo "Question 2:" . "<br>";
questioneTwo("AAABBB");
echo "<hr>";

function questioneThree($arr){
    $swap = 0;
    for($i=0; $i<count($arr); $i++){
        if( $arr[$i] != $i+1 ){
            $temp = $arr[$i];
            $arr[$i+1] = $temp;
            $arr[$i] = $i+1;
            $swap++;
        }
    }
    print_r($arr);
    echo "<br>" . $swap-1;
}
echo "Question 3:" . "<br>";
questioneThree([7,1,3,2,4,5,6]);
